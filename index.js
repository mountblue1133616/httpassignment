var http = require(`http`);
var fs = require("fs");
var url = require(`url`);
const { v4: uuidv4 } = require("uuid");

const handleMultipleRequests = (req, res) => {
  const statusCode = Number(req.url.split("/")[2]);
  const delaySeconds = Number(req.url.split("/")[2]);

  if (req.method === `GET` && req.url === `/html`) {
    res.setHeader(`Content-type`, `text/html`);
    res.write(`<!DOCTYPE html>
        <html>
        <head>
        </head>
        <body>
        <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
        <p> - Martin Fowler</p>
        
        </body>
        </html>`);
    res.end("Read HTML file");
  } else if (req.method === "GET" && req.url === `/json`) {
    res.setHeader(`Content-type`, `application/json`);
    res.write(`
    {
        "slideshow": {
            "author": "Yours Truly",
            "date": "date of publication",
            "slides": [
                {
                    "title": "Wake up to WonderWidgets!",
              "type": "all"
            },
            {
              "items": [
                "Why <em>WonderWidgets</em> are great",
                "Who <em>buys</em> WonderWidgets"
              ],
              "title": "Overview",
              "type": "all"
            }
          ],
          "title": "Sample Slide Show"
        }
      }`);
    res.end();
  } else if (req.method === "GET" && req.url === `/uuid`) {
    res.setHeader(`Content-type`, `application/json`);
    res.write(JSON.stringify({ uuid: uuidv4() }));
    res.end();
  } else if (req.method === "GET" && req.url === `/status/${statusCode}`) {
    res.statuscode = statusCode;
    res.end(`Status code ${statusCode}`);
  } else if (req.method === `GET` && req.url === `/delay/${delaySeconds}`) {
    setTimeout(() => {
      res.end(`Delayed by ${delaySeconds} succesffully`);
    }, delaySeconds * 1000);
  } else {
    res.statusCode = 404;
    res.end("Error");
  }
};

const server = http.createServer(handleMultipleRequests);

server.listen(3008, () => {
  console.log("Server is listening on port 3008");
});
